package com.dbeshenov;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {
    public static void main(String[] args) {
        if (args.length < 2) return;

        SquareManager squareManager = new SquareManager();

        try(FileInputStream fin = new FileInputStream(args[0])) {
            RectangleReader rectangleReader = new RectangleReader(fin);

            while(rectangleReader.hasNext()){
                squareManager.addRectangle(rectangleReader.next());
            }
        }
        catch (IOException ex){
            System.out.print(ex.getMessage());
        }

        try(FileOutputStream fout = new FileOutputStream(args[1])) {
            PrintWriter pw = new PrintWriter(fout);
            pw.println(squareManager.calcSquare());
            pw.flush();
        }
        catch (IOException ex){
            System.out.print(ex.getMessage());
        }
    }
}
