package com.dbeshenov;

import com.dbeshenov.model.Rectangle;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Scanner;

/**
 * Created by Dmitry Beshenov on 23.08.2015.
 */
public final class RectangleReader implements Closeable {
    private Scanner source;

    public RectangleReader(InputStream source) {
        this.source = new Scanner(source);
    }

    @Override
    public void close() throws IOException {
        source.close();
    }

    public Rectangle next() throws IOException {
        int x1 = tryReadInt();
        int y1 = tryReadInt();
        int x2 = tryReadInt();
        int y2 = tryReadInt();
        return new Rectangle(x1, y1, x2, y2);
    }

    public boolean hasNext() {
        return source.hasNextInt();
    }

    private int tryReadInt() throws IOException  {
        if(source.hasNextInt()) {
            return source.nextInt();
        }
        else throw new FormatException("Wrong rectangle format");
    }

    public class FormatException extends IOException {
        public FormatException() {
        }

        public FormatException(String message) {
            super(message);
        }

        public FormatException(String message, Throwable cause) {
            super(message, cause);
        }

        public FormatException(Throwable cause) {
            super(cause);
        }
    }
}
