package com.dbeshenov;

import com.dbeshenov.model.PointProjection;
import com.dbeshenov.model.Rectangle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Dmitry Beshenov on 23.08.2015.
 */
public class SquareManager {
    private List<PointProjection> xProjection;
    private List<Rectangle> rectangles;

    public SquareManager() {
        this.xProjection = new ArrayList<>();
        this.rectangles = new ArrayList<>();
    }

    public void addRectangle(Rectangle rectangle) {
        rectangles.add(rectangle);
        xProjection.add(new PointProjection(true, rectangle.getP1().getX()));
        xProjection.add(new PointProjection(false, rectangle.getP2().getX()));
    }

    public long calcSquare(){
        Collections.sort(xProjection);
        long commonSquare = 0;
        int lineSign = 0;
        long xLine;
        boolean isLastX = false;
        PointProjection xPoint;

        for(int i = 0; i < xProjection.size() - 1; i++) {
            lineSign += xProjection.get(i).getLineSign();
            if (lineSign != 0) {
                xLine = xProjection.get(i+1).getCoordinate() - xProjection.get(i).getCoordinate();
                if (i == xProjection.size() - 2) {
                    xPoint = xProjection.get(i+1);
                    isLastX = true;
                } else {
                    xPoint = xProjection.get(i);
                }
                long yLines = calcLinesSum(findYProjections(xPoint, isLastX));
                commonSquare += xLine*yLines;
            }
        }
        return commonSquare;
    }

    private List<PointProjection> findYProjections(PointProjection xprojection, boolean isLastX) {
        List<PointProjection> result = new ArrayList<>();
        for(Rectangle rect: rectangles) {
            boolean isEdge = (isLastX ? xprojection.getCoordinate() == rect.getP2().getX() : xprojection.getCoordinate() == rect.getP1().getX());

            if (xprojection.getCoordinate() < rect.getP2().getX() &&
                    xprojection.getCoordinate() > rect.getP1().getX() || isEdge) {
                result.add(new PointProjection(true, rect.getP1().getY()));
                result.add(new PointProjection(false, rect.getP2().getY()));
            }
        }
        Collections.sort(result);
        return result;
    }

    private int calcLinesSum(List<PointProjection> projections) {
        int lines = 0;
        int lineSign = 0;
        for(int i = 0; i < projections.size() - 1; i++) {
            lineSign += projections.get(i).getLineSign();
            if (lineSign != 0) {
                lines += projections.get(i+1).getCoordinate() - projections.get(i).getCoordinate();
            }
        }
        return lines;
    }
}
