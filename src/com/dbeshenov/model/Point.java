package com.dbeshenov.model;

/**
 * Created by Dmitry Beshenov on 23.08.2015.
 */
public class Point implements Comparable<Point> {
    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public int compareTo(Point point) {
        if(x != point.getX()) return x - point.getX();
        else return y - point.getY();
    }

    // region getters and setters

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    // endregion
}
