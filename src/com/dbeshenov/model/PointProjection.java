package com.dbeshenov.model;

/**
 * Created by Dmitry Beshenov on 23.08.2015.
 */
public class PointProjection implements Comparable<PointProjection> {
    private int coordinate;
    private boolean isStart;

    public PointProjection(boolean isStart, int coordinate) {
        this.isStart = isStart;
        this.coordinate = coordinate;
    }

    @Override
    public int compareTo(PointProjection o) {
        return coordinate - o.getCoordinate();
    }

    // region getters and setters

    public int getLineSign() {
        if (isStart) return 1;
        else return -1;
    }

    public int getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(int coordinate) {
        this.coordinate = coordinate;
    }

    public boolean isStart() {
        return isStart;
    }

    public boolean isEnd() {
        return !isStart;
    }

    public void setIsStart(boolean isStart) {
        this.isStart = isStart;
    }

    // endregion
}
