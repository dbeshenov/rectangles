package com.dbeshenov.model;

/**
 * Created by Dmitry Beshenov on 23.08.2015.
 */
public class Rectangle {
    private Point p1;
    private Point p2;

    public Rectangle(Point p1, Point p2) {
        this.p1 = new Point(Math.min(p1.getX(), p2.getX()), Math.min(p1.getY(), p2.getY()));
        this.p2 = new Point(Math.max(p1.getX(), p2.getX()), Math.max(p1.getY(), p2.getY()));
    }

    public Rectangle(int x1, int y1, int x2, int y2) {
        this.p1 = new Point(Math.min(x1, x2), Math.min(y1, y2));
        this.p2 = new Point(Math.max(x1, x2), Math.max(y1, y2));
    }

    // region getters and setters

    public Point getP1() {
        return p1;
    }

    public void setP1(Point p1) {
        this.p1 = p1;
    }

    public Point getP2() {
        return p2;
    }

    public void setP2(Point p2) {
        this.p2 = p2;
    }

    // endregion
}
