package com.dbeshenov;

import com.dbeshenov.model.Point;
import com.dbeshenov.model.Rectangle;
import junit.framework.TestCase;

/**
 * Created by Dmitry Beshenov on 23.08.2015.
 */
public class SquareManagerTest extends TestCase {

    public void testFindSquare1Rect() throws Exception {
        SquareManager squareManager = new SquareManager();
        squareManager.addRectangle(new Rectangle(new Point(1, 1), new Point(7, 7)));

        long square = squareManager.calcSquare();
        assertEquals(36, square);
    }

    public void testFindSquare2Rect() throws Exception {
        SquareManager squareManager = new SquareManager();
        squareManager.addRectangle(new Rectangle(new Point(1, 1), new Point(3, 3)));
        squareManager.addRectangle(new Rectangle(new Point(2, 2), new Point(4, 4)));

        long square = squareManager.calcSquare();
        assertEquals(7, square);
    }

    public void testFindSquare2IntersectRect() throws Exception {
        SquareManager squareManager = new SquareManager();
        squareManager.addRectangle(new Rectangle(new Point(1, 1), new Point(5, 5)));
        squareManager.addRectangle(new Rectangle(new Point(6, 2), new Point(3, -1)));

        long square = squareManager.calcSquare();
        assertEquals(23, square);
    }

    public void testFindSquareComplex() throws Exception {
        SquareManager squareManager = new SquareManager();
        squareManager.addRectangle(new Rectangle(new Point(-12,  5), new Point(-10,  3)));
        squareManager.addRectangle(new Rectangle(new Point(-11,  4), new Point(  7,  6)));
        squareManager.addRectangle(new Rectangle(new Point( -7, -3), new Point( -6,  8)));
        squareManager.addRectangle(new Rectangle(new Point( -8, -1), new Point( 13,  1)));
        squareManager.addRectangle(new Rectangle(new Point(  2,  2), new Point(  5,  6)));
        squareManager.addRectangle(new Rectangle(new Point(  4, -2), new Point( 11,  4)));
        squareManager.addRectangle(new Rectangle(new Point(  7,  1), new Point(  9,  3)));
        squareManager.addRectangle(new Rectangle(new Point( 10,-11), new Point( 12, -6)));
        squareManager.addRectangle(new Rectangle(new Point( 15,  2), new Point( 17,  4)));
        squareManager.addRectangle(new Rectangle(new Point( 15,  2), new Point( 17,  4)));
        squareManager.addRectangle(new Rectangle(new Point(  0, -2), new Point( 13, -4)));
        long square = squareManager.calcSquare();
        assertEquals(160, square);
    }

    public void testFindSquareComplex2() throws Exception {
        SquareManager squareManager = new SquareManager();
        squareManager.addRectangle(new Rectangle(-11, 4, -10, 3));
        squareManager.addRectangle(new Rectangle(-11, 5, -8, 2));
        squareManager.addRectangle(new Rectangle(-9, -4, -8, 3));
        squareManager.addRectangle(new Rectangle(-8, 2, -4, 5));
        squareManager.addRectangle(new Rectangle(-10, -3, 1, -2));
        squareManager.addRectangle(new Rectangle(-5, -4, -3, 4));
        squareManager.addRectangle(new Rectangle(-3, 1, 4, 2));
        squareManager.addRectangle(new Rectangle(-2, 4, 2, 7));
        squareManager.addRectangle(new Rectangle(1, -5, 6, 12));
        squareManager.addRectangle(new Rectangle(3, -1, 5, -3));
        long square = squareManager.calcSquare();
        assertEquals(9+12+7+5+12+8+9+5*17, square);
    }
}
